from typing import Any, Dict
from django import forms
from django.contrib.auth import get_user_model


class SignUpForm(forms.ModelForm):
    username = forms.CharField(min_length=4, max_length=30, widget=forms.TextInput)
    email = forms.EmailField(widget=forms.EmailInput)
    password = forms.CharField(min_length=8, max_length=30, widget=forms.PasswordInput)
    confirm_password = forms.CharField(
        min_length=8, max_length=30, widget=forms.PasswordInput
    )

    class Meta:
        model = get_user_model()
        fields = ["username", "email", "password"]

    def clean(self) -> Dict[str, Any]:
        cleaned_data = super().clean()

        # Password validation via repeating password.
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        if password and confirm_password and password != confirm_password:
            self.add_error("password", "Passwords do not match.")
            self.add_error("confirm_password", "Passwords do not match.")

        # Only alphanumeric.
        username = cleaned_data.get("username")
        if not str(username).isalnum():
            self.add_error("username", "Please enter only alphanumerics.")

        return cleaned_data


class SignInForm(forms.Form):
    login_cred = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
