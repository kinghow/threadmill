from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import get_user_model


class SignUpAdmin(UserAdmin):
    model = get_user_model()
    # Lists fields when adding a user.
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide"),
                "fields": (
                    "slug",
                    "username",
                    "email",
                    "password1",
                    "password2",
                    "is_staff",
                    "is_active",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
    )
    list_display = ("username", "email", "is_staff", "is_active", "date_joined", "slug")


admin.site.register(get_user_model(), SignUpAdmin)
