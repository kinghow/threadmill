from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.text import slugify
from unidecode import unidecode


class User(AbstractUser):
    email = models.EmailField(max_length=50, primary_key=True)
    # username is the displayed name.
    username = models.CharField(max_length=30, unique=True)
    password = models.CharField(max_length=30)
    slug = models.SlugField(default="", null=False)

    def save(self, *args, **kwargs):
        self.slug = slugify(unidecode(self.username))
        super().save(*args, **kwargs)

    def __str__(self) -> str:
        return self.slug
