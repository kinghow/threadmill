from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView, RedirectView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from .forms import SignUpForm, SignInForm
from posts.models import Post, Comment, Vote
from django.db.models import Q


class SignUpView(FormView):
    template_name = "accounts/sign_up.html"
    form_class = SignUpForm
    success_url = reverse_lazy("accounts:sign_in")

    def form_valid(self, form: SignUpForm) -> HttpResponse:
        user = form.save(commit=False)
        # Obscure password before saving into database.
        user.set_password(form.cleaned_data["password"])
        user.save()
        messages.success(self.request, "Sign up successful!")
        return super().form_valid(form)


class SignInView(FormView):
    template_name = "accounts/sign_in.html"
    form_class = SignInForm
    success_url = reverse_lazy("posts:index")

    def form_valid(self, form: SignInForm) -> HttpResponse:
        login_cred = form.cleaned_data.get("login_cred")
        password = form.cleaned_data.get("password")

        # authenticate overriden by custom authentication backend.
        user = authenticate(self.request, username=login_cred, password=password)

        if user is not None:
            login(self.request, user)
            return super().form_valid(form)
        else:
            form.add_error("login_cred", "Incorrect login details.")
            form.add_error("password", "Incorrect login details.")
            return super().form_invalid(form)


class SignOutView(RedirectView):
    url = reverse_lazy("posts:index")

    def get_redirect_url(self, *args, **kwargs):
        logout(self.request)
        messages.success(self.request, "Signed out.")
        return super().get_redirect_url(*args, **kwargs)


class ProfileView(LoginRequiredMixin, DetailView):
    login_url = reverse_lazy("accounts:sign_in")
    model = get_user_model()
    template_name = "accounts/profile.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        user = self.get_object()

        posts = Post.objects.filter(author=user).order_by("-pub_date")
        context["posts"] = posts

        # Get comment count for each post.
        comment_count_list = {}
        for post in posts:
            comment_count_list[post.id] = post.comments.count()
        context["comment_count_list"] = comment_count_list

        comments = Comment.objects.filter(author=user).order_by("-pub_date")
        context["comments"] = comments

        votes = Vote.objects.filter(voter=user).order_by("-date_order")
        context["votes"] = votes

        # Make sure the user context holds the authenticated user instead of the object.
        context["user"] = self.request.user

        return context
