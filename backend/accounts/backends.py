from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model
from django.db.models import Q


class EmailOrUsernameAuthenticationBackend(ModelBackend):
    """
    Allow user authentication by email or username.
    """

    def authenticate(self, request, username=None, password=None):
        user_model = get_user_model()

        try:
            user = user_model.objects.get(Q(username=username) | Q(email=username))
        except user_model.DoesNotExist:
            return None

        if user.check_password(password):
            return user

        return None
