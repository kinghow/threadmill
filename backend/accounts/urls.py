from django.urls import path
from .views import SignUpView, SignInView, SignOutView, ProfileView


app_name = "accounts"
urlpatterns = [
    path("signup/", SignUpView.as_view(), name="sign_up"),
    path("signin/", SignInView.as_view(), name="sign_in"),
    path("signout/", SignOutView.as_view(), name="sign_out"),
    path("profile/<slug:slug>", ProfileView.as_view(), name="profile"),
]
