from django.urls import path
from .views import (
    PostCreateView,
    PostIndexView,
    PostDetailView,
    voteView,
    PostEditView,
    CommentEditView,
    PostDeleteView,
    CommentDeleteView,
    SearchView,
)


app_name = "posts"
urlpatterns = [
    path("", PostIndexView.as_view(), name="index"),
    path("threads/new/", PostCreateView.as_view(), name="create"),
    path("threads/<int:pk>/", PostDetailView.as_view(), name="detail"),
    path(
        "threads/<int:post_id>/<int:id>/<int:content_type>/<int:vote_type>/<str:redir>/",
        voteView,
        name="vote",
    ),
    path("threads/<int:pk>/edit/", PostEditView.as_view(), name="edit"),
    path(
        "threads/<int:t_pk>/<int:pk>/edit/",
        CommentEditView.as_view(),
        name="edit_comment",
    ),
    path("threads/<int:pk>/delete/", PostDeleteView.as_view(), name="delete"),
    path(
        "threads/<int:t_pk>/<int:pk>/delete/",
        CommentDeleteView.as_view(),
        name="delete_comment",
    ),
    path("search/", SearchView.as_view(), name="search"),
]
