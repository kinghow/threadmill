from django.dispatch import receiver
from django.db.models.signals import pre_delete
from django.db.models import F
from .models import Vote


@receiver(pre_delete, sender=Vote)
def delete_vote_signal(sender, instance, **kwargs):
    # Calculate the vote rating every time an object is voted.
    instance.voted_object.vote_rating = (
        F("vote_rating") - 1 if instance.vote_type else F("vote_rating") + 1
    )
    instance.voted_object.save()
