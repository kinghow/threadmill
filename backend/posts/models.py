from typing import Any, Dict, Tuple
from django.db import models
from django.db.models import F
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from datetime import datetime


# Post is referred to as thread on the website.
class Post(models.Model):
    title = models.CharField(max_length=140)
    content = models.TextField(max_length=500)
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    pub_date = models.DateTimeField(verbose_name="date published", auto_now_add=True)

    # Inverse relationship for Vote.
    votes = GenericRelation("Vote", related_query_name="vote")

    # Inverse relationship for Comment.
    comments = GenericRelation("Comment", related_query_name="comments")

    # Convenience field for calculating ratings from all the votes.
    vote_rating = models.IntegerField(default=0)

    def __str__(self) -> str:
        return str(self.id) + ": " + self.title


class Comment(models.Model):
    content = models.TextField(max_length=500)
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    pub_date = models.DateTimeField(verbose_name="date published", auto_now_add=True)

    # Inverse relationship for Vote.
    votes = GenericRelation("Vote", related_query_name="vote")

    # Use generic relation so that a parent can either be a post or a comment.
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    # object_id is the post or comment id.
    object_id = models.PositiveIntegerField()
    parent_object = GenericForeignKey("content_type", "object_id")

    # Inverse relationship for Comment.
    comments = GenericRelation("Comment", related_query_name="comments")

    # Convenience field for calculating ratings from all the votes.
    vote_rating = models.IntegerField(default=0)

    class Meta:
        # Add index to generic relation for fast inverse lookup.
        indexes = [
            models.Index(fields=["content_type", "object_id"]),
        ]

    def __str__(self) -> str:
        object_str = "Post" if self.content_type.model == "post" else "Comment"

        return (
            str(self.id)
            + ": "
            + self.author.username
            + "'s comment on "
            + object_str
            + " #"
            + str(self.object_id)
        )


class Vote(models.Model):
    # True for upvote, false for downvote.
    vote_type = models.BooleanField()
    voter = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    # Use generic relation so that a vote can either be a post or a comment.
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    # object_id is the post or comment id.
    object_id = models.PositiveIntegerField()
    voted_object = GenericForeignKey("content_type", "object_id")

    # For convenient ordering.
    date_order = models.DateTimeField(default=datetime.now)

    class Meta:
        # Add index to generic relation for fast inverse lookup.
        indexes = [
            models.Index(fields=["content_type", "object_id"]),
        ]
        # A voter can only vote a post or a comment once.
        unique_together = ("voter", "content_type", "object_id")

    def save(self, *args, **kwargs):
        # Calculate the vote rating every time an object is voted.

        # Vote object already exists.
        if self.pk is not None:
            self.voted_object.vote_rating = (
                F("vote_rating") + 2 if self.vote_type else F("vote_rating") - 2
            )
        # Vote object doesn't exist.
        else:
            self.voted_object.vote_rating = (
                F("vote_rating") + 1 if self.vote_type else F("vote_rating") - 1
            )
            self.date_order = self.voted_object.pub_date
        self.voted_object.save()

        super().save(*args, **kwargs)

    def __str__(self) -> str:
        object_str = "Post" if self.content_type.model == "post" else "Comment"

        return (
            self.voter.username
            + "'s vote on "
            + object_str
            + " #"
            + str(self.object_id)
        )
