from django.template import Library
from django.template.defaulttags import register

register = Library()


@register.filter
def dict_lookup(dictionary, key):
    return dictionary.get(key)


@register.filter
def get_type(value):
    return type(value).__name__


@register.filter
def get_comment_parent_object_id(comment_list, key):
    return comment_list.get(id=key).parent_object.id


@register.filter
def get_comment_parent_object_title(comment_list, key):
    return comment_list.get(id=key).parent_object.title
