from django.contrib import admin
from .models import Post, Comment, Vote


class PostAdmin(admin.ModelAdmin):
    fields = ["author", "title", "content", "vote_rating"]
    list_display = ("id", "author", "title", "pub_date", "vote_rating")
    list_display_links = ["title"]


# class CommentAdmin(admin.ModelAdmin):
#     fields = ["author", "content"]
#     list_display = ("id", "author", "pub_date")
#     list_display_links = ["author"]


admin.site.register(Post, PostAdmin)
admin.site.register(Comment)
admin.site.register(Vote)
