from typing import Any
from django.db.models.query import QuerySet
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.views.generic import (
    TemplateView,
    ListView,
    DetailView,
    FormView,
    UpdateView,
    DeleteView,
)
from django.views.generic.edit import FormMixin
from django.contrib import messages
from django.contrib.auth import get_user_model
from .models import Post, Comment, Vote
from .forms import (
    PostDetailForm,
    PostCreateForm,
    PostEditForm,
    CommentEditForm,
    SearchForm,
)
from django.db.models import Q
from django.db import models
from django.contrib.auth.mixins import LoginRequiredMixin


def get_has_voted_data(post_or_comment_list, user=None):
    """
    Helper function to check if user has voted. Given a list of posts or a list of comments, returns
    a list of upvote and downvote html class based on user vote status.
    """

    upvote_class_list = {}
    downvote_class_list = {}

    for object in post_or_comment_list:
        upvote_class_list[object.id] = "link-secondary"
        downvote_class_list[object.id] = "link-secondary"

        if user is not None:
            # Check if the user has voted.
            if user.is_authenticated:
                try:
                    vote = object.votes.get(voter__exact=user)
                    if vote:
                        if vote.vote_type:
                            upvote_class_list[object.id] = "link-success"
                        else:
                            downvote_class_list[object.id] = "link-danger"
                except:
                    pass

    return upvote_class_list, downvote_class_list


class PostIndexView(ListView):
    template_name = "posts/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        user = self.request.user

        # Get votes data.
        upvotes, downvotes = get_has_voted_data(self.object_list, user)
        context["upvote_class"] = upvotes
        context["downvote_class"] = downvotes

        # Get comment count for each post.
        comment_count_list = {}
        for post in self.object_list:
            comment_count_list[post.id] = post.comments.count()
        context["comment_count_list"] = comment_count_list

        return context

    def get_queryset(self) -> QuerySet[Any]:
        # Most popular first.
        return Post.objects.order_by("-vote_rating", "-pub_date")


class PostCreateView(FormView):
    template_name = "posts/post_create.html"
    form_class = PostCreateForm
    success_url = reverse_lazy("posts:index")

    def form_valid(self, form: PostCreateForm) -> HttpResponse:
        if self.request.user.is_authenticated:
            post = form.save(commit=False)
            post.author = self.request.user
            post.save()
            return super().form_valid(form)
        else:
            form.add_error("title", "Please sign in to post.")
            return super().form_invalid(form)


class PostDetailView(FormMixin, DetailView):
    model = Post
    template_name = "posts/post_detail.html"
    form_class = PostDetailForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        user = self.request.user

        # Get votes data for the post.
        upvotes, downvotes = get_has_voted_data([self.object], user)
        context["upvote_class"] = upvotes[self.object.pk]
        context["downvote_class"] = downvotes[self.object.pk]

        # Get votes data for the all the comments.
        upvotes, downvotes = get_has_voted_data(self.object.comments.all(), user)
        context["comments_upvote_class"] = upvotes
        context["comments_downvote_class"] = downvotes

        # Get all comments.
        context["comments"] = self.object.comments.order_by("-vote_rating", "-pub_date")

        return context

    def get_success_url(self):
        return reverse("posts:detail", kwargs={"pk": self.object.pk})

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()

        form = self.get_form()
        if form.is_valid() and self.request.user.is_authenticated:
            return self.form_valid(form)
        else:
            form.add_error("content", "Please sign in to comment.")
            return self.form_invalid(form)

    def form_valid(self, form: PostDetailForm) -> HttpResponse:
        if self.request.user.is_authenticated:
            comment = form.save(commit=False)
            comment.parent_object = Post.objects.get(pk=self.kwargs.get("pk"))
            comment.author = self.request.user
            comment.save()
            return super().form_valid(form)
        else:
            form.add_error("content", "Please sign in to comment.")
            return super().form_invalid(form)


def voteView(request, post_id, id, content_type, vote_type, redir):
    # content_type 0 = Post, 1 = Comment
    if not content_type:
        object = get_object_or_404(Post, pk=id)
    else:
        object = get_object_or_404(Comment, pk=id)

    user = request.user
    if user.is_authenticated:
        # Don't allow self-voting.
        if user == object.author:
            messages.error(request, "You can vote for yourself!")
            if redir == "posts:detail":
                return redirect(reverse(redir, args=(post_id,)))
            else:
                return redirect(reverse(redir))

        try:
            vote = object.votes.get(voter__exact=user)
            # The user has already voted.

            # Upvoted.
            if vote.vote_type:
                if vote_type:
                    # User already upvoted. So, remove the vote.
                    vote.delete()
                else:
                    # Change to downvote.
                    vote.vote_type = False
                    vote.save()
            # Downvoted.
            else:
                if not vote_type:
                    # User already downvoted. So, remove the vote.
                    vote.delete()
                else:
                    # Change to upvote.
                    vote.vote_type = True
                    vote.save()

            if redir == "posts:detail":
                return redirect(reverse(redir, args=(post_id,)))
            else:
                return redirect(reverse(redir))
        except (KeyError, Vote.DoesNotExist):
            # The user haven't voted.
            new_vote = Vote(vote_type=bool(vote_type), voter=user, voted_object=object)
            new_vote.save()

            if redir == "posts:detail":
                return redirect(reverse(redir, args=(post_id,)))
            else:
                return redirect(reverse(redir))
    else:
        # User not logged in.
        messages.error(request, "You must be logged in to do that.")
        if redir == "posts:detail":
            return redirect(reverse(redir, args=(post_id,)))
        else:
            return redirect(reverse(redir))


class PostEditView(UpdateView):
    model = Post
    template_name = "posts/post_edit.html"
    form_class = PostEditForm

    def get_success_url(self, **kwargs):
        return reverse_lazy("posts:detail", kwargs={"pk": self.kwargs.get("pk")})

    def form_valid(self, form: PostEditForm) -> HttpResponse:
        if self.request.user.is_authenticated:
            if self.request.user == self.object.author:
                self.object.save()
                return super().form_valid(form)
            else:
                form.add_error("title", "You don't own the thread.")
                form.add_error("content", "You don't own the thread.")
                return super().form_invalid(form)
        else:
            form.add_error("title", "Please sign in to edit.")
            return super().form_invalid(form)


class CommentEditView(UpdateView):
    model = Comment
    template_name = "posts/comment_edit.html"
    form_class = CommentEditForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["post"] = Post.objects.get(id=self.kwargs.get("t_pk"))
        return context

    def get_success_url(self, **kwargs):
        return reverse_lazy("posts:detail", kwargs={"pk": self.kwargs.get("t_pk")})

    def form_valid(self, form: CommentEditForm) -> HttpResponse:
        if self.request.user.is_authenticated:
            if self.request.user == self.object.author:
                self.object.save()
                return super().form_valid(form)
            else:
                form.add_error("title", "You don't own the comment.")
                form.add_error("content", "You don't own the comment.")
                return super().form_invalid(form)
        else:
            form.add_error("title", "Please sign in to edit.")
            return super().form_invalid(form)


class PostDeleteView(LoginRequiredMixin, DeleteView):
    login_url = reverse_lazy("accounts:sign_in")
    model = Post
    success_url = reverse_lazy("posts:index")

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.error(request, "You must be logged in to do that.")
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form: Any) -> HttpResponse:
        # the Post object
        object = self.get_object()
        if object.author == self.request.user:
            success_url = self.get_success_url()
            object.delete()
            return redirect(success_url)
        else:
            messages.error(self.request, "The thread does not belong to you.")
            return redirect(reverse("posts:detail", args=(self.kwargs.get("pk"),)))


class CommentDeleteView(LoginRequiredMixin, DeleteView):
    login_url = reverse_lazy("accounts:sign_in")
    model = Comment

    def get_success_url(self, **kwargs):
        return reverse_lazy("posts:detail", kwargs={"pk": self.kwargs.get("t_pk")})

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.error(request, "You must be logged in to do that.")
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form: Any) -> HttpResponse:
        # the Post object
        object = self.get_object()
        if object.author == self.request.user:
            success_url = self.get_success_url()
            object.delete()
            return redirect(success_url)
        else:
            messages.error(self.request, "The comment does not belong to you.")
            return redirect(reverse("posts:detail", args=(self.kwargs.get("t_pk"),)))


class SearchView(FormMixin, TemplateView):
    template_name = "posts/search.html"
    form_class = SearchForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        search_string = self.request.GET.get("search")
        context["form"] = SearchForm(initial={"search": search_string})

        posts = Post.objects.filter(
            Q(title__contains=search_string) | Q(content__contains=search_string)
        )
        # context["posts"] = posts
        comments = Comment.objects.filter(content__contains=search_string)
        # Need for getting comment parent.
        context["comments"] = comments

        # Adjust columns for union.
        posts_selected = posts.values(
            "id", "author", "pub_date", "content", "vote_rating", "title"
        )
        comments_selected = (
            comments.defer("content_type", "object_id", "parent_object")
            .values("id", "author", "pub_date", "content", "vote_rating")
            .annotate(title=models.Value(None, models.CharField()))
        )

        hits = posts_selected.union(comments_selected).order_by("-pub_date")
        for hit in hits:
            hit["author"] = get_user_model().objects.get(email=hit["author"])
        context["hits"] = hits

        # Get comment count for each post.
        comment_count_list = {}
        for post in posts:
            comment_count_list[post.id] = post.comments.count()
        context["comment_count_list"] = comment_count_list

        context["user"] = self.request.user

        return context
