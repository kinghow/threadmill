from django import forms
from .models import Post, Comment


class PostCreateForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ["title", "content"]

    def clean(self):
        cleaned_data = super().clean()

        # Unallow spaces only.
        title = cleaned_data.get("title")
        if len(str(title).strip()) == 0:
            self.add_error("title", "Please fill in the title.")

        # Unallow spaces only.
        content = cleaned_data.get("content")
        if len(str(content).strip()) == 0:
            self.add_error("content", "Please fill in the content.")

        return cleaned_data


class PostDetailForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ["content"]

    def clean(self):
        cleaned_data = super().clean()

        # Unallow spaces only.
        content = cleaned_data.get("content")
        if len(str(content).strip()) == 0:
            self.add_error("content", "Please fill in the content.")

        return cleaned_data


class PostEditForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ["title", "content"]

    def clean(self):
        cleaned_data = super().clean()

        # Unallow spaces only.
        title = cleaned_data.get("title")
        if len(str(title).strip()) == 0:
            self.add_error("title", "Please fill in the title.")

        # Unallow spaces only.
        content = cleaned_data.get("content")
        if len(str(content).strip()) == 0:
            self.add_error("content", "Please fill in the content.")

        return cleaned_data


class CommentEditForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ["content"]

    def clean(self):
        cleaned_data = super().clean()

        # Unallow spaces only.
        content = cleaned_data.get("content")
        if len(str(content).strip()) == 0:
            self.add_error("content", "Please fill in the content.")

        return cleaned_data


class SearchForm(forms.Form):
    search = forms.CharField(max_length=80)
