# ThreadMill

## Summary

ThreadMill is a simple forum site made with [Django](https://www.djangoproject.com/). Site features include:
- Account creation
- Thread browsing
- Thread/Comment creation, editing and deletion
- Voting system
- Account profile viewing
- Thread/Comment searching
- Anonymous browsing
- Various validations from form clearning to url blocking
- ... and many more to come!

### Browsing
![Browsing](doc/browsing.gif)

### Posting
![Posting](doc/thread-and-comment-posting.gif)

### Profile Viewing
![Profile Viewing](doc/profile-viewing.gif)

### Searching
![Searching](doc/searching.gif)

## How to Run
1. Install [Python](https://www.python.org/downloads/release/python-3114/).
2. At terminal, run `python ./backend/manage.py runserver` from repository root.
3. Go to `http://127.0.0.1:8000` on any web browser.